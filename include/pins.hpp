//SPDX-License-Identifier: GPL-3.0-only
#pragma once
#include <inttypes.h>

// Address pins
const uint8_t addr0 = 16;
const uint8_t addr1 = 15;
const uint8_t addr2 = 14;
const uint8_t addr3 = 12;
const uint8_t addr4 = 11;
const uint8_t addr5 = 10;
const uint8_t addr6 = 9;
const uint8_t addr7 = 8;
const uint8_t addr8 = 7;
const uint8_t addr9 = 6;
const uint8_t addr10 = 5;
const uint8_t addr11 = 4;
const uint8_t addr12 = 3;
const uint8_t addr13 = 2;
const uint8_t addr14 = 1;
const uint8_t addr15 = 0;

// Data Pins
const uint8_t data0 = 25;
const uint8_t data1 = 26;
const uint8_t data2 = 27;
const uint8_t data3 = 28;
const uint8_t data4 = 29;
const uint8_t data5 = 30;
const uint8_t data6 = 31;
const uint8_t data7 = 32;

// Control pins
const uint8_t M1 = 17;
const uint8_t RESET = 18;
const uint8_t CLOCK = 19;
const uint8_t INT = 20;
const uint8_t MREQ = 21;
const uint8_t WEn = 22;
const uint8_t RDn = 23;
const uint8_t IOREQ = 24;
