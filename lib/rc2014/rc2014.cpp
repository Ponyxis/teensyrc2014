//SPDX-License-Identifier: GPL-3.0-only
#include <rc2014.hpp>
#include <Arduino.h>

RC2014 rc2014;

void RC2014::SetupAddressPin() {
    pinMode(addr0, OUTPUT);
    pinMode(addr1, OUTPUT);
    pinMode(addr2, OUTPUT);
    pinMode(addr3, OUTPUT);
    pinMode(addr4, OUTPUT);
    pinMode(addr5, OUTPUT);
    pinMode(addr6, OUTPUT);
    pinMode(addr7, OUTPUT);
    pinMode(addr8, OUTPUT);
    pinMode(addr9, OUTPUT);
    pinMode(addr10, OUTPUT);
    pinMode(addr11, OUTPUT);
    pinMode(addr12, OUTPUT);
    pinMode(addr13, OUTPUT);
    pinMode(addr14, OUTPUT);
    pinMode(addr15, OUTPUT);
}

void RC2014::SetupDataOutputPin() {
    pinMode(data0, OUTPUT);
    pinMode(data1, OUTPUT);
    pinMode(data2, OUTPUT);
    pinMode(data3, OUTPUT);
    pinMode(data4, OUTPUT);
    pinMode(data5, OUTPUT);
    pinMode(data6, OUTPUT);
    pinMode(data7, OUTPUT);
}

void RC2014::SetupDataInputPin() {
    pinMode(data0, INPUT);
    pinMode(data1, INPUT);
    pinMode(data2, INPUT);
    pinMode(data3, INPUT);
    pinMode(data4, INPUT);
    pinMode(data5, INPUT);
    pinMode(data6, INPUT);
    pinMode(data7, INPUT);
}

void RC2014::SetAddress(uint16_t address) {
    digitalWriteFast(addr0, (address & 1) ? HIGH : LOW);
    digitalWriteFast(addr1, (address & 2) ? HIGH : LOW);
    digitalWriteFast(addr2, (address & 4) ? HIGH : LOW);
    digitalWriteFast(addr3, (address & 8) ? HIGH : LOW);
    digitalWriteFast(addr4, (address & 16) ? HIGH : LOW);
    digitalWriteFast(addr5, (address & 32) ? HIGH : LOW);
    digitalWriteFast(addr6, (address & 64) ? HIGH : LOW);
    digitalWriteFast(addr7, (address & 128) ? HIGH : LOW);
    digitalWriteFast(addr8, (address & 256) ? HIGH : LOW);
    digitalWriteFast(addr9, (address & 512) ? HIGH : LOW);
    digitalWriteFast(addr10, (address & 1024) ? HIGH : LOW);
    digitalWriteFast(addr11, (address & 2048) ? HIGH : LOW);
    digitalWriteFast(addr12, (address & 4096) ? HIGH : LOW);
    digitalWriteFast(addr13, (address & 8192) ? HIGH : LOW);
    digitalWriteFast(addr14, (address & 16384) ? HIGH : LOW);
    digitalWriteFast(addr15, (address & 32768) ? HIGH : LOW);
}

void RC2014::SetData(uint8_t data) {
    digitalWriteFast(data0, (data & 1) ? HIGH : LOW);
    digitalWriteFast(data1, (data & 2) ? HIGH : LOW);
    digitalWriteFast(data2, (data & 4) ? HIGH : LOW);
    digitalWriteFast(data3, (data & 8) ? HIGH : LOW);
    digitalWriteFast(data4, (data & 16) ? HIGH : LOW);
    digitalWriteFast(data5, (data & 32) ? HIGH : LOW);
    digitalWriteFast(data6, (data & 64) ? HIGH : LOW);
    digitalWriteFast(data7, (data & 128) ? HIGH : LOW);
}

uint8_t RC2014::GetData() {
    byte b = 0;

    if (digitalRead(data0) == HIGH) b |= 1;
    if (digitalRead(data1) == HIGH) b |= 2;
    if (digitalRead(data2) == HIGH) b |= 4;
    if (digitalRead(data3) == HIGH) b |= 8;
    if (digitalRead(data4) == HIGH) b |= 16;
    if (digitalRead(data5) == HIGH) b |= 32;
    if (digitalRead(data6) == HIGH) b |= 64;
    if (digitalRead(data7) == HIGH) b |= 128;

    return(b);
}
