//SPDX-License-Identifier: GPL-3.0-only
#pragma once
#include <pins.hpp>

class RC2014{
    public:
        void SetupAddressPin();
        void SetupDataOutputPin();
        void SetupDataInputPin();
        void SetAddress(uint16_t address);
        void SetData(uint8_t data);
        uint8_t GetData();

};

extern RC2014 rc2014;
